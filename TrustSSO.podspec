Pod::Spec.new do |spec|
    spec.name                         = 'TrustSSO'
    spec.module_name                  = 'TrustSSO'
    spec.version                      = '1.9.38'
    spec.license                      = { :type => "MIT", :file => "LICENSE" }
    spec.homepage                     = 'https://gitlab.com/trustchile/ios-public-frameworks/trustsso-xcframework'
    spec.authors                      = { 'Benjamin Cáceres' => 'bcaceres@trust.lat' }
    spec.summary                      = 'TrustSSO.'
    spec.source                       = { :git => 'https://gitlab.com/trustchile/ios-public-frameworks/trustsso-xcframework.git', :tag => "#{spec.version}" }
    spec.platform                     = :ios
    spec.swift_version                = '5.0'
    spec.ios.deployment_target        = '14.0'
    spec.requires_arc                 = true
    spec.frameworks                   = 'UIKit'
	spec.vendored_frameworks          = 'TrustSSO.xcframework'
	spec.dependency                   'Sentry'
end
